package nl.alvant.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "akb:faqitem")
@Node(jcrType = "akb:faqitem")
public class FaqItem extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "akb:question")
    public String getQuestion() {
        return getSingleProperty("akb:question");
    }

    @HippoEssentialsGenerated(internalName = "akb:answer")
    public HippoHtml getAnswer() {
        return getHippoHtml("akb:answer");
    }

    @HippoEssentialsGenerated(internalName = "hippostd:tags")
    public String[] getTags() {
        return getMultipleProperty("hippostd:tags");
    }
}
