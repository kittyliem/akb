package nl.alvant.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageBean;

@HippoEssentialsGenerated(internalName = "akb:imageset")
@Node(jcrType = "akb:imageset")
public class Imageset extends HippoGalleryImageSet {
    @HippoEssentialsGenerated(internalName = "akb:bannerLarge")
    public HippoGalleryImageBean getBannerLarge() {
        return getBean("akb:bannerLarge", HippoGalleryImageBean.class);
    }

    @HippoEssentialsGenerated(internalName = "akb:oblong")
    public HippoGalleryImageBean getOblong() {
        return getBean("akb:oblong", HippoGalleryImageBean.class);
    }

    @HippoEssentialsGenerated(internalName = "akb:smallSquare")
    public HippoGalleryImageBean getSmallSquare() {
        return getBean("akb:smallSquare", HippoGalleryImageBean.class);
    }

    @HippoEssentialsGenerated(internalName = "akb:mediumsquare")
    public HippoGalleryImageBean getMediumsquare() {
        return getBean("akb:mediumsquare", HippoGalleryImageBean.class);
    }

    @HippoEssentialsGenerated(internalName = "akb:large")
    public HippoGalleryImageBean getLarge() {
        return getBean("akb:large", HippoGalleryImageBean.class);
    }
}
