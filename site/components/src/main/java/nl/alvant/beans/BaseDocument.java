package nl.alvant.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;

@Node(jcrType="akb:basedocument")
public class BaseDocument extends HippoDocument {

}
