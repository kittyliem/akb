package nl.alvant.beans;

import java.util.Calendar;
import java.util.List;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.components.model.Authors;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "akb:blogpost")
@Node(jcrType = "akb:blogpost")
public class Blogpost extends HippoDocument implements Authors {
    public static final String TITLE = "akb:title";
    public static final String INTRODUCTION = "akb:introduction";
    public static final String CONTENT = "akb:content";
    public static final String PUBLICATION_DATE = "akb:publicationdate";
    public static final String CATEGORIES = "akb:categories";
    public static final String AUTHOR = "akb:author";
    public static final String AUTHOR_NAMES = "akb:authornames";
    public static final String LINK = "akb:link";
    public static final String AUTHORS = "akb:authors";
    public static final String TAGS = "hippostd:tags";

    @HippoEssentialsGenerated(internalName = "akb:publicationdate")
    public Calendar getPublicationDate() {
        return getSingleProperty(PUBLICATION_DATE);
    }

    @HippoEssentialsGenerated(internalName = "akb:authornames")
    public String[] getAuthorNames() {
        return getMultipleProperty(AUTHOR_NAMES);
    }

    public String getAuthor() {
        final String[] authorNames = getAuthorNames();
        if (authorNames != null && authorNames.length > 0) {
            return authorNames[0];
        }
        return null;
    }

    @HippoEssentialsGenerated(internalName = "akb:title")
    public String getTitle() {
        return getSingleProperty(TITLE);
    }

    @HippoEssentialsGenerated(internalName = "akb:content")
    public HippoHtml getContent() {
        return getHippoHtml(CONTENT);
    }

    @HippoEssentialsGenerated(internalName = "akb:introduction")
    public String getIntroduction() {
        return getSingleProperty(INTRODUCTION);
    }

    @HippoEssentialsGenerated(internalName = "akb:categories")
    public String[] getCategories() {
        return getMultipleProperty(CATEGORIES);
    }

    @Override
    @HippoEssentialsGenerated(internalName = "akb:authors")
    public List<Author> getAuthors() {
        return getLinkedBeans(AUTHORS, Author.class);
    }

    @HippoEssentialsGenerated(internalName = "hippostd:tags")
    public String[] getTags() {
        return getMultipleProperty(TAGS);
    }
}
