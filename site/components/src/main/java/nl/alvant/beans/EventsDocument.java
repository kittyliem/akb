package nl.alvant.beans;

import java.util.Calendar;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "akb:eventsdocument")
@Node(jcrType = "akb:eventsdocument")
public class EventsDocument extends HippoDocument {
    /** 
     * The document type of the events document.
     */
    public static final String DOCUMENT_TYPE = "akb:eventsdocument";
    private static final String TITLE = "akb:title";
    private static final String DATE = "akb:date";
    private static final String INTRODUCTION = "akb:introduction";
    private static final String IMAGE = "akb:image";
    private static final String CONTENT = "akb:content";
    private static final String LOCATION = "akb:location";
    private static final String END_DATE = "akb:enddate";

    /** 
     * Get the title of the document.
     * @return the title
     */
    @HippoEssentialsGenerated(internalName = "akb:title")
    public String getTitle() {
        return getSingleProperty(TITLE);
    }

    /** 
     * Get the date of the document, i.e. the start date of the event.
     * @return the (start) date
     */
    @HippoEssentialsGenerated(internalName = "akb:date")
    public Calendar getDate() {
        return getSingleProperty(DATE);
    }

    /** 
     * Get the introduction of the document.
     * @return the introduction
     */
    @HippoEssentialsGenerated(internalName = "akb:introduction")
    public String getIntroduction() {
        return getSingleProperty(INTRODUCTION);
    }

    /** 
     * Get the main content of the document.
     * @return the content
     */
    @HippoEssentialsGenerated(internalName = "akb:content")
    public HippoHtml getContent() {
        return getHippoHtml(CONTENT);
    }

    /** 
     * Get the location of the document.
     * @return the location
     */
    @HippoEssentialsGenerated(internalName = "akb:location")
    public String getLocation() {
        return getSingleProperty(LOCATION);
    }

    /** 
     * Get the end date of the document, i.e. the end date of the event.
     * @return the end date
     */
    @HippoEssentialsGenerated(internalName = "akb:enddate")
    public Calendar getEndDate() {
        return getSingleProperty(END_DATE);
    }

    @HippoEssentialsGenerated(internalName = "akb:image")
    public Imageset getImage() {
        return getLinkedBean(IMAGE, Imageset.class);
    }

    @HippoEssentialsGenerated(internalName = "hippostd:tags")
    public String[] getTags() {
        return getMultipleProperty("hippostd:tags");
    }
}
