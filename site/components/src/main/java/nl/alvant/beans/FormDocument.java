package nl.alvant.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;

@HippoEssentialsGenerated(internalName = "akb:formDocument")
@Node(jcrType = "akb:formDocument")
public class FormDocument extends BasicDocument {
}
