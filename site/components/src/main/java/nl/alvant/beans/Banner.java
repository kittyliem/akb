package nl.alvant.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import nl.alvant.beans.Imageset;

@HippoEssentialsGenerated(internalName = "akb:bannerdocument")
@Node(jcrType = "akb:bannerdocument")
public class Banner extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "akb:title")
    public String getTitle() {
        return getSingleProperty("akb:title");
    }

    @HippoEssentialsGenerated(internalName = "akb:content")
    public HippoHtml getContent() {
        return getHippoHtml("akb:content");
    }

    @HippoEssentialsGenerated(internalName = "akb:link")
    public HippoBean getLink() {
        return getLinkedBean("akb:link", HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "akb:image")
    public Imageset getImage() {
        return getLinkedBean("akb:image", Imageset.class);
    }
}
