package nl.alvant.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import java.util.Calendar;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import nl.alvant.beans.Imageset;

@HippoEssentialsGenerated(internalName = "akb:basicDocument")
@Node(jcrType = "akb:basicDocument")
public class BasicDocument extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "akb:title")
    public String getTitle() {
        return getSingleProperty("akb:title");
    }

    @HippoEssentialsGenerated(internalName = "akb:shortIntroduction")
    public String getShortIntroduction() {
        return getSingleProperty("akb:shortIntroduction");
    }

    @HippoEssentialsGenerated(internalName = "akb:datePublished")
    public Calendar getDatePublished() {
        return getSingleProperty("akb:datePublished");
    }

    @HippoEssentialsGenerated(internalName = "akb:introduction")
    public HippoHtml getIntroduction() {
        return getHippoHtml("akb:introduction");
    }

    @HippoEssentialsGenerated(internalName = "akb:image")
    public Imageset getImage() {
        return getLinkedBean("akb:image", Imageset.class);
    }
}
