package nl.alvant.beans;

import java.util.List;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.onehippo.cms7.essentials.components.model.AuthorEntry;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import nl.alvant.beans.Imageset;

@HippoEssentialsGenerated(internalName = "akb:author")
@Node(jcrType = "akb:author")
public class Author extends HippoDocument implements AuthorEntry {
    public static final String ROLE = "akb:role";
    public static final String ACCOUNTS = "akb:accounts";
    public static final String FULL_NAME = "akb:fullname";
    public static final String IMAGE = "akb:image";
    public static final String CONTENT = "akb:content";

    @HippoEssentialsGenerated(internalName = "akb:fullname")
    public String getFullName() {
        return getSingleProperty(FULL_NAME);
    }

    @HippoEssentialsGenerated(internalName = "akb:content")
    public HippoHtml getContent() {
        return getHippoHtml(CONTENT);
    }

    @HippoEssentialsGenerated(internalName = "akb:role")
    public String getRole() {
        return getSingleProperty(ROLE);
    }

    @HippoEssentialsGenerated(internalName = "akb:accounts")
    public List<Account> getAccounts() {
        return getChildBeansByName(ACCOUNTS, Account.class);
    }

    @HippoEssentialsGenerated(internalName = "akb:image")
    public Imageset getImage() {
        return getLinkedBean("akb:image", Imageset.class);
    }
}
