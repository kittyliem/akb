package nl.alvant.beans;

import java.util.Calendar;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "akb:contentdocument")
@Node(jcrType = "akb:contentdocument")
public class ContentDocument extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "akb:introduction")
    public String getIntroduction() {
        return getSingleProperty("akb:introduction");
    }

    @HippoEssentialsGenerated(internalName = "akb:title")
    public String getTitle() {
        return getSingleProperty("akb:title");
    }

    @HippoEssentialsGenerated(internalName = "akb:content")
    public HippoHtml getContent() {
        return getHippoHtml("akb:content");
    }

    @HippoEssentialsGenerated(internalName = "akb:publicationdate")
    public Calendar getPublicationDate() {
        return getSingleProperty("akb:publicationdate");
    }

    @HippoEssentialsGenerated(internalName = "hippostd:tags")
    public String[] getTags() {
        return getMultipleProperty("hippostd:tags");
    }
}
