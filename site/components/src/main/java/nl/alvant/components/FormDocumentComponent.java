package nl.alvant.components;

import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.onehippo.cms7.essentials.components.EssentialsContentComponent;

public class FormDocumentComponent extends EssentialsContentComponent {


    @Override
    public void doBeforeRender(HstRequest request, HstResponse response) {
        super.doBeforeRender(request, response);

        final HstRequestContext context = request.getRequestContext();
        final HippoBean bean = context.getContentBean();

        request.setAttribute("document", bean);
    }
}
