<#include "../include/imports.ftl">

<#-- @ftlvariable name="pageable" type="org.onehippo.cms7.essentials.components.paging.Pageable" -->
<#-- @ftlvariable name="item" type="nl.alvant.beans.ContentDocument" -->
<#if pageable?? && pageable.items?has_content>
  <div>
    <#list pageable.items as item>
      <div class="has-edit-button">
        <@hst.manageContent hippobean=item/>
        <@hst.link var="link" hippobean=item/>
        <div class="col col-sm-offset-1">
          <h2 class="heading-2"><a href="${link}">${item.title?html}</a></h2>
        </div>
        <#if item.publicationDate??>
          <div class="col col-sm-offset-1">
            <@fmt.formatDate value=item.publicationDate.time type="both" dateStyle="medium" timeStyle="short"/>
          </div>
        </#if>
        <#if item.introduction??>
          <div class="col col-sm-offset-1">
            ${item.introduction?html}
          </div>
        </#if>
      </div>
    </#list>
    <div class="has-new-content-button">
      <@hst.manageContent documentTemplateQuery="new-content-document" rootPath="content"/>
    </div>
    <#if cparam.showPagination>
      <#include "../include/pagination.ftl">
    </#if>
  </div>
<#-- @ftlvariable name="editMode" type="java.lang.Boolean"-->
<#elseif editMode>
  <div>
    <img src="<@hst.link path='/images/essentials/catalog-component-icons/generic-list.svg'/>"> Click to edit Content list
    <div class="has-new-content-button">
      <@hst.manageContent documentTemplateQuery="new-content-document" rootPath="content"/>
    </div>
  </div>
</#if>
