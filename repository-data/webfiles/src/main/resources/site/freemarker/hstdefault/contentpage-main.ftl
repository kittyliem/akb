<#include "../include/imports.ftl">

<#-- @ftlvariable name="document" type="nl.alvant.beans.ContentDocument" -->
<#if document??>
  <div class="container">
    <div class="col col-md-offset-1 col-md-9">
      <@hst.manageContent hippobean=document />
      <h1 class="heading-1">${document.title?html}</h1>
      <#if document.publicationDate??>
        <p class="date-small">
          <@fmt.formatDate value=document.publicationDate.time type="both" dateStyle="medium" timeStyle="short"/>
        </p>
      </#if>
      <#if document.introduction??>
        <p>${document.introduction?html}</p>
      </#if>
      <@hst.html hippohtml=document.content/>
    </div>
<#-- @ftlvariable name="editMode" type="java.lang.Boolean"-->
<#elseif editMode>
  <div class="has-edit-button">
    <img src="<@hst.link path="/images/essentials/catalog-component-icons/simple-content.svg" />"> Click to edit Simple Content
    <@hst.manageContent documentTemplateQuery="new-content-document" parameterName="document" rootPath="content"/>
  </div>
  </div>
</#if>