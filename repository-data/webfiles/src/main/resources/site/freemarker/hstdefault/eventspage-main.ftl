<#include "../include/imports.ftl">

<#-- @ftlvariable name="document" type="nl.alvant.beans.EventsDocument" -->
<#if document??>
  <@hst.link var="link" hippobean=document/>
  <div class="container has-edit-button">
    <@hst.manageContent hippobean=document/>
    <div class="col col-md-offset-3 col-md-9">
    <h1 class="heading-1"><a href="${link}">${document.title?html}</a></h1>
      <#if document.date??>
        <p class="date-big"><@fmt.formatDate value=document.date.time type="both" dateStyle="medium" timeStyle="short"/></p>
      </#if>
      <#if document.endDate??>
        <p><@fmt.formatDate value=document.endDate.time type="both" dateStyle="medium" timeStyle="short"/></p>
      </#if>
      <#if document.location??>
        <p>${document.location?html}</p>
      </#if>
      <#if document.introduction??>
        <p>${document.introduction?html}</p>
      </#if>
      <#if document.image?? && document.image.large??>
        <@hst.link var="img" hippobean=document.image.large/>
        <figure>
          <img src="${img?html}" title="${document.image.fileName?html}" alt="${document.image.fileName?html}"/>
            <#if document.image.description??>
              <figcaption>${document.image.description?html}</figcaption>
            </#if>
        </figure>
      </#if>
      <@hst.html hippohtml=document.content/>
    </div>
  </div>
</#if>