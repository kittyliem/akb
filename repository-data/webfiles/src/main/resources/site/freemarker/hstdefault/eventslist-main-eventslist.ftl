<#include "../include/imports.ftl">

<#-- @ftlvariable name="item" type="nl.alvant.beans.EventsDocument" -->
<#-- @ftlvariable name="pageable" type="org.onehippo.cms7.essentials.components.paging.Pageable" -->
<div class="body-wrapper">
    <div class="container">

            <div class="col-md-offset-1">
          <#if pageable?? && pageable.items?has_content>
              <#list pageable.items as item>
                  <@hst.link var="link" hippobean=item />
                <div class="row">
                        <div class="col col-sm-offset-1 col-1 col-md-2 col-sm-2">
                          <div class="box">
                          <#if item.image?? && item.image.smallSquare??>
                              <@hst.link var="img" hippobean=item.image.smallSquare/>
                              <h2 class="heading-2">
                                  <a href="${link}">
                                      <img src="${img}" alt="${item.title?html}"></a>
                              </h2>
                          </#if>
                          </div>
                        </div>
                        <div class="col col-2 col-md-6 col-sm-6 text-right">
                              <@hst.manageContent hippobean=item />
                              <h2 class="heading-2"><a href="${link}">${item.title?html}</a></h2>
                              <div>
                                  <#if item.date?? && item.date.time??>
                                    <p><@fmt.formatDate value=item.date.time type="both" dateStyle="medium" timeStyle="short"/></p>
                                  </#if>
                                  <#if item.endDate?? && item.endDate.time??>
                                    <p><@fmt.formatDate value=item.endDate.time type="both" dateStyle="medium" timeStyle="short"/></p>
                                  </#if>
                                  <p>${item.location?html}</p>
                              </div>
                              <div class="text-2">
                                <p>${item.introduction?html}</p>
                              </div>
                          </div>
                </div>
              </#list>
              <#if cparam.showPagination>
                  <#include "../include/pagination.ftl">
              </#if>
          <#-- @ftlvariable name="editMode" type="java.lang.Boolean"-->
          <#elseif editMode>
            <div>
              <img src="<@hst.link path='/images/essentials/catalog-component-icons/events-list.svg'/>"> Click to edit Event List
              <div class="has-new-content-button">
                <@hst.manageContent documentTemplateQuery="new-events-document" folderTemplateQuery="new-events-folder" rootPath="events" defaultPath="${currentYear}/${currentMonth}"/>
              </div>
            </div>
          </#if>
            </div>

    </div>
</div>

