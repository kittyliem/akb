<#include "../../include/imports.ftl">

<#-- @ftlvariable name="pageable" type="org.onehippo.cms7.essentials.components.paging.Pageable" -->
<@hst.setBundle basename="essentials.blog"/>
<#if pageable??>
<div>
  <#list pageable.items as item>
    <@hst.link var="link" hippobean=item />
    <div class="has-edit-button">
        <@hst.manageContent hippobean=item/>
        <h2 class="heading-2"><a href="${link}">${item.title?html}</a></h2>
        <#if item.publicationDate?? && item.publicationDate.time??>
          <p class="date-small">
            <@fmt.formatDate value=item.publicationDate.time type="both" dateStyle="medium" timeStyle="short"/></p>
        </#if>
        <p>${item.introduction?html}</p>
        <p><a href="${link}"><@fmt.message key="blog.read.post" var="msg"/>${msg?html}</a></p>
    </div>
  </#list>
  <div class="has-new-content-button">
    <@hst.manageContent documentTemplateQuery="new-blog-document" folderTemplateQuery="new-blog-folder" rootPath="blog" defaultPath="${currentYear}/${currentMonth}"/>
  </div>
  <#if cparam.showPagination>
    <#include "../../include/pagination.ftl">
  </#if>
</div>
<#-- @ftlvariable name="editMode" type="java.lang.Boolean"-->
<#elseif editMode>
<div>
  <img src="<@hst.link path='/images/essentials/catalog-component-icons/blog-list.svg'/>"> Click to edit Blog List
</div>
</#if>