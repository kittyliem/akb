<!doctype html>
<#include "../../include/imports.ftl">
<html lang="en" xmlns="http://www.w3.org/1999/html">
<#include "head.ftl"/>
<body>

<#--<#include "navbar.ftl"/>-->

<div class="layout container">
<#--    <@hst.include ref="breadcrumb"/>-->

    <div>
        <@hst.include ref="header"/>
    </div>

    <div class="row">
        <div class="col-lg-8 col-12">
            <div class="section section-pb">
                <@hst.include ref="main"/>
            </div>
        </div>
        <div class="col-lg-4 col-12 sidebar-right">
            <div class="section section-pt">
                <@hst.include ref="sidebar-right"/>
            </div>
            </div>
        </div>


        <@hst.include ref="footer"/>

    </div>

    <@hst.headContributions categoryIncludes="htmlBodyEnd, scripts" xhtml=true/>

</body>
</html>
