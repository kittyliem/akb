<#include "../include/imports.ftl">

<@hst.setBundle basename="essentials.global"/>

<footer class="footer-block">
  <div class="container">
    <div class="row">
      <div class="col-md-offset-1 col-md-3" style="">
        <div class="icon-wrapper share">
          <div class="row">
            <div class="col-lg-4">
              <a href="https://www.facebook.com/Alvant-336028023400953/" target="_blank">
                <img src="<@hst.webfile path="/images/logos/393277_facebook_icon.png"/>" alt=""/>
              </a>
            </div>
            <div class="col-lg-offset-3 col-lg-4">
              <a href="https://www.linkedin.com/company/17916579/" target="_blank">
                <img src="<@hst.webfile path="/images/logos/1233018_linkedin_icon.png"/>" alt=""/>
              </a>
            </div>
          </div>
        </div>
      </div>
        <div class="col-2 col-md-4" style="">
          <div class="text-6">
            <p><span style="font-family: cairo-light; font-weight: 300;">Veraartlaan 12</span><br>
               <span style="font-family: cairo-light; font-weight: 300;">2288 GM Rijswijk</span><br>
               <span style="font-family: cairo-light; font-weight: 300;">Nederland</span></p>
          </div>
        </div>
        <div class="col-3 col-md-3" style="">
          <div class="text-6">
            <p><span style="font-family: cairo-light; font-weight: 300;">T. <a href="tel:0884482060">+31 (0)88 44 82 060</a></span><br>
               <span style="font-family: cairo-light; font-weight: 300;">E. <span><a style="font-family: cairo-light; font-weight: 300;" href="mailto:info@alvant.nl">info@alvant.nl</a></span></span><br>
               <span style="font-family: cairo-light; font-weight: 300;">W. <span><a href="http://www.alvant.nl" rel="noopener" target="_blank">www.alvant.nl</a></span>
               </span>
            </p>
          </div>
        </div>
    </div>
  </div>
</footer>
<section class="page-footer-block">
  <div class="container">
    <div class="row">
      <div class="col align-self-end" style="">
        <div class="text-6">
          <p><span style="font-family: cairo-light; font-weight: 300;">Alvant © 2021 |&nbsp;
              <span style="color: cornflowerblue; text-decoration: underline;"><a href="/contact/disclaimer/">disclaimer</a></span>&nbsp;|&nbsp;
              <span style="color: cornflowerblue; text-decoration: underline;"><a href="/contact/privacy-statement/">privacy statement</a></span>
            </span></p>
        </div>
      </div>
    </div>
  </div>
</section>