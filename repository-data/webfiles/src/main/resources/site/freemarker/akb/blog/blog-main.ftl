<#include "../../include/imports.ftl">

<div class="container">
    <div class="row">
        <div class="col-sm-offset-1 col-sm-6">
            <@hst.include ref="detailcontainer"/>
        </div>
        <div class="col-sm-offset-1 col-sm-3">
            <@hst.include ref="sidebarcontainer"/>
        </div>
    </div>
</div>