<#include "../include/imports.ftl">

<#-- @ftlvariable name="document" type="nl.alvant.beans.FaqItem" -->
<#if document??>
  <div class="container">
    <div class="has-edit-button" >
      <@hst.manageContent hippobean=document/>
      <div class="col col-sm-offset-1">
        <h1 class="heading-1">${document.question?html}</h1>
      </div>
      <div class="col col-sm-offset-1">
        <@hst.html hippohtml=document.answer />
      </div>
    </div>
  </div>
</#if>


