<#include "../include/imports.ftl">

<@hst.setBundle basename="loginpage"/>
<div class="col col-md-offset-3 col-md-9">
  <h1 class="heading-1"><@fmt.message key="login.title" var="title"/>${title}</h1>
  <p><@fmt.message key="login.text" var="text"/>${text}</p>

  <form class="form-1" action="<@hst.link path='/'/>login" method="post">
    <#if param.error != null>
      <p>
        Invalid username and password.
      </p>
    </#if>
    <#if param.logout != null>
      <p>
        You have been logged out.
      </p>
    </#if>
    <div class="form-field-1">
      <label for="username">Username *</label>
      <input type="text" id="username" name="username" required class="input-lg"/>
    </div>
    <div class="form-field-1">
      <label for="password">Password *</label>
      <input type="password" id="password" name="password" required/>
    </div>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <div class="button-wrapper">
        <button type="submit" class="button-2 bg-blue">Log in</button>
    </div>
  </form>

</div>
