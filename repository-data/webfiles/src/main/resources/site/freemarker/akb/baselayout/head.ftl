<!doctype html>
<head>
    <title>Alvant KennisBank</title>
    <meta charset="utf-8"/>
    <link rel="preload" href="<@hst.webfile  path="/css/bootstrap.min.css"/>" type="text/css" as="style"/>
    <link rel="stylesheet" href="<@hst.webfile  path="/css/akb.css"/>" type="text/css" as="style"/>
    <link rel="stylesheet" href="<@hst.webfile  path="/css/fonts.css"/>" type="text/css" as="style"/>
    <link rel="stylesheet" href="<@hst.webfile  path="/css/colors.css"/>" type="text/css" as="style"/>
    <link rel="stylesheet" href="<@hst.webfile  path="/css/responsive.css"/>" type="text/css" as="style"/>
    <link rel="apple-touch-icon" href="<@hst.webfile path="/images/apple-touch-icon.png"/>" />
    <#if hstRequest.requestContext.channelManagerPreviewRequest>
        <link rel="stylesheet" href="<@hst.webfile  path="/css/cms-request.css"/>" type="text/css"/>
    </#if>
    <@hst.headContributions categoryExcludes="htmlBodyEnd, scripts" xhtml=true/>
    <@hst.headContributions categoryIncludes="htmlHead" xhtml=true/>
</head>