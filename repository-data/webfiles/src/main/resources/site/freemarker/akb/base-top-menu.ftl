<#include "../include/imports.ftl">

<div class="navbar">
    <div class="container">
        <div class="row">
            <!-- Logo -->
            <div class="col-md-offset-1 col-sm-3">
                <a href="./"><img src="<@hst.webfile path="/images/logos/logo_2021_alvant.jpg"/>" alt="" width="244" height="58" /></a>
            </div>
            <!-- //Logo// -->
            <div class="col-sm-5">
                <#if menu??>
                    <#if menu.siteMenuItems??>
                        <ul>
                            <#list menu.siteMenuItems as item>
                                <#if item.selected || item.expanded>
                                    <li class="active"><a href="<@hst.link link=item.hstLink/>" class="activelink"><span class="label-nav">${item.name?html}</span> </a></li>
                                <#else>
                                    <li><a href="<@hst.link link=item.hstLink/>"><span class="label-nav">${item.name?html}</span></a></li>
                                </#if>
                            </#list>
                        </ul>
                    </#if>
                    <@hst.cmseditmenu menu=menu/>
                </#if>

            </div>
            <#-- Logout must be by POST (see http://docs.spring.io/spring-security/site/docs/current/reference/htmlsingle/#csrf-logout) -->
            <div class="col-sm-3">
                <form name="logoutForm" method="post" action="<@hst.link path="/"/>logout">
                    <p>
                        <#if hstRequest.userPrincipal??>
                            Welcome <strong>${hstRequest.userPrincipal.name}</strong>!
                            <#if _csrf??>
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            </#if>
                            <button class="button-2 bg-blue" type="submit" name="logoutButton">Log out</button>
                        </#if>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>
