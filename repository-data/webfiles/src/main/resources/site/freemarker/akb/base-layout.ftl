<!doctype html>
<#include "../include/imports.ftl">
<!-- version: 0.2.0 -->
<html class="no-js" lang="en">
<head>
  <#include "baselayout/head.ftl"/>
</head>
  <body>
      <div class="page">
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <@hst.include ref="top"/>
          </div>
        </div>

        <@hst.include ref="menu"/>

        <div class="body-container">
          <div class="container">
            <@hst.include ref="main"/>
          </div>
        </div>

        <@hst.include ref="footer"/>

      </div>

    <script src="<@hst.webfile path="/js/jquery-3.5.1.min.js"/>" type="text/javascript"></script>
    <script src="<@hst.webfile path="/js/bootstrap.min.js"/>" type="text/javascript"></script>
    <script src="<@hst.webfile path="/js/alvant.js"/>" type="text/javascript"></script>

    <@hst.headContributions categoryIncludes="htmlBodyEnd, scripts" xhtml=true/>
  </body>
</html>