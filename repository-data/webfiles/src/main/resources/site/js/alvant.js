var Cookie = function(options) {
    this.init(options);
};
Cookie.prototype = {
    init: function(options) {
        this.options = {
            selector: '[data-cookie]'
        };
        $.extend(this.options, options);
        this.cookie_nodes = $(this.options.selector);
        this.start();
    },
    start: function() {
        var match;
        if (match = /(?:^|[\s;])cookie_(hide|agree)=(\d)/.exec(document.cookie)) {
            if (match[1] == 'agree' && match[2] == 1) {
                var script_nodes = $('script[type="text/plain"]');
                var iframe_nodes = $('iframe[data-src]');
                script_nodes.each(function() {
                    var script_node = $(this);
                    var script_html = script_node.html();
                    var _script_node = $('<script>', {
                        html: script_html
                    });
                    script_node.replaceWith(_script_node);
                });
                iframe_nodes.each(function() {
                    var iframe_node = $(this);
                    var iframe_src = iframe_node.data('src');
                    iframe_node.attr('src', iframe_src).removeData('src');
                });
            }
        } else {
            this.cookie_nodes.addClass('show');
            this.setEvents();
        }
    },
    setEvents: function() {
        var _this = this;
        this.cookie_nodes.find('[data-action="hide"]').on('click', function() {
            document.cookie = 'cookie_hide=1;path=/;expires=' + _this.getExpiryDate();
            _this.hide();
        });
        this.cookie_nodes.find('[data-action="agree"]').on('click', function() {
            document.cookie = 'cookie_agree=1;path=/;expires=' + _this.getExpiryDate();
            location.reload();
        });
        this.cookie_nodes.find('[data-action="disagree"]').on('click', function() {
            document.cookie = 'cookie_agree=0;path=/';
            _this.hide();
        });
    },
    hide: function() {
        this.cookie_nodes.each(function() {
            var cookie_node = $(this);
            if (cookie_node.parents('#sticky').length) {
                var sticky_index = $('#sticky > .page').children().index(cookie_node);
                $('#viewport > .page > .ghost:eq(' + sticky_index + ')').hide();
            }
            cookie_node.hide();
        });
    },
    getExpiryDate: function() {
        var date = new Date();
        date.setTime(date.getTime() + (365 * 24 * 60 * 60 * 1000));
        return date.toUTCString();
    }
};
var Menu = function(options) {
    this.init(options);
};
Menu.prototype = {
    init: function(options) {
        this.options = {
            selector: '.menu',
            mobile: false,
            mobile_size: 0,
            aside: true,
            init: true,
            bottom: false,
            line_text: false
        };
        $.extend(this.options, options);
        this.window_node = $(window);
        this.body_node = $('body');
        this.navigation_node = $('#navigation, [id^="navigation-"]');
        this.sticky_node = $('#sticky');
        this.viewport_node = $('#viewport');
        this.page_node = $();
        this.block_node = $();
        this.col_node = $();
        this.menu_node = $();
        this.handler_node = $();
        this.listener_node = $();
        this.outer_node = $();
        this.inner_node = $();
        this.item_nodes = $();
        this.anchor_nodes = $();
        this.viewport_top = this.viewport_node.length ? this.viewport_node.offset().top : 0;
        this.set_mobile = false;
        this.mobile = false;
        this.mobile_size = this.options.mobile_size;
        this.position = null;
        this.scrollbar_width = 0;
        this.listener_width = 0;
        this.transition = typeof document.body.style.transition !== 'undefined';
        this.hold = false;
        this.start();
    },
    start: function() {
        var _this = this;
        var selectors = this.options.selector.split(',');
        if (selectors.length > 1) {
            for (var x in selectors) {
                var options = $.extend({}, this.options);
                options.selector = $.trim(selectors[x]);
                new Menu(options);
            }
        } else {
            var menu_nodes = $(this.options.selector);
            menu_nodes.each(function(index) {
                if (index) {
                    var options = $.extend({}, _this.options);
                    options.selector += ':eq(' + index + ')';
                    options.init = false;
                    new Menu(options);
                } else {
                    _this.menu_node = $(this);
                    _this.page_node = _this.menu_node.parents('.page:first');
                    _this.block_node = _this.menu_node.parents('header, section, footer').first();
                    _this.col_node = _this.menu_node.parents('.col:first');
                    _this.handler_node = _this.menu_node.find('.handler:first');
                    _this.listener_node = _this.menu_node.find('.mobile-menu:first');
                    _this.outer_node = _this.listener_node.children('.outer:first');
                    _this.inner_node = _this.outer_node.children('.inner:first');
                    _this.item_nodes = _this.inner_node.find('li:has(ul)');
                    _this.anchor_nodes = _this.inner_node.find('a[href^="#"]');
                    var img_nodes = _this.block_node.find('img');
                    if (img_nodes.length) {
                        var counter = 0;
                        img_nodes.each(function() {
                            $(this).on('load', function() {
                                counter++;
                                if (counter == img_nodes.length) {
                                    _this.setMobile();
                                }
                            });
                        });
                    }
                    _this.setMobile();
                    _this.setSubPosition(true);
                    _this.setEvents();
                    _this.menu_node.addClass('init');
                }
            });
        }
    },
    setMobile: function(force) {
        var window_width = this.window_node.width();
        if (this.listener_node.hasClass('active')) {
            this.set_mobile = true;
            if (this.options.aside) {
                this.viewport_node.add(this.navigation_node).add(this.sticky_node).css('transition', '');
            }
            this.listener_node.removeClass('active');
        } else if (!this.set_mobile) {
            force = true;
        }
        if (!this.transition) {
            this.transitionEnd();
        }
        if (!this.transition || force) {
            if (!this.options.mobile && !this.options.mobile_size) {
                if (window_width > this.mobile_size) {
                    this.col_node.addClass('measure');
                    this.col_node.siblings('.col').css('flex-shrink', 0);
                    this.inner_node.addClass('nowrap');
                    if (this.mobile) {
                        this.menu_node.removeClass('mobile');
                        var menu_width = this.menu_node.width();
                        var inner_width = this.inner_node.outerWidth();
                        this.menu_node.addClass('mobile');
                    } else {
                        var menu_width = this.menu_node.width();
                        var inner_width = this.inner_node.outerWidth();
                    }
                    this.inner_node.removeClass('nowrap');
                    this.col_node.siblings('.col').css('flex-shrink', '');
                    this.col_node.removeClass('measure');
                    if (inner_width > menu_width) {
                        this.mobile_size = window_width;
                    }
                }
            }
            if (this.options.mobile || this.mobile_size) {
                if (this.options.mobile || window_width <= this.mobile_size) {
                    this.menu_node.addClass('mobile');
                    if (!this.position) {
                        this.setPosition();
                    }
                    this.mobile = true;
                } else {
                    this.menu_node.removeClass('mobile');
                    this.mobile = false;
                }
            }
        }
    },
    setPosition: function() {
        if (this.viewport_node.length && this.handler_node.length) {
            var menu_display = this.menu_node.css('display');
            if (this.options.aside) {
                if (this.col_node.prev('.col').length) {
                    this.position = 'right';
                } else if (this.col_node.next('.col').length) {
                    this.position = 'left';
                } else {
                    var handler_width = this.handler_node.width();
                    var handler_left = this.handler_node.offset().left;
                    var handler_center = handler_left + (handler_width / 2);
                    var page_width = this.viewport_node.width();
                    var page_left = this.viewport_node.offset().left;
                    var page_center = page_left + (page_width / 2);
                    this.position = handler_center < page_center ? 'left' : 'right';
                }
            } else {
                this.position = 'center';
            }
            if (menu_display !== 'table-cell') {
                this.menu_node.addClass(this.position);
            }
            this.listener_node.addClass(this.position);
        }
    },
    setSubPosition: function(init) {
        var _this = this;
        if (this.item_nodes.length) {
            var spacer_height = 0;
            if (this.options.bottom) {
                var col_bottom = this.col_node.offset().top + this.col_node.outerHeight();
                var item_top = this.item_nodes.first().offset().top;
                var list_top = col_bottom - item_top;
                spacer_height = list_top - this.item_nodes.first().outerHeight();
            }
            this.item_nodes.each(function() {
                var item_node = $(this);
                var list_node = item_node.children('ul:first');
                var list_margin_top = parseInt(list_node.css('margin-top'));
                var list_margin_left = parseInt(list_node.css('margin-left'));
                if (_this.options.bottom || list_margin_top || list_margin_left) {
                    var spacer_node = item_node.children('.spacer:first');
                    if (!spacer_node.length) {
                        spacer_node = $('<div>', {
                            class: 'spacer'
                        });
                        list_node.before(spacer_node);
                    }
                    if (spacer_height || list_margin_top) {
                        spacer_node.css('height', spacer_height + list_margin_top);
                    }
                    if (list_margin_left) {
                        spacer_node.css('width', list_margin_left);
                    }
                    if (_this.options.bottom && !item_node.parents('li:first').length) {
                        list_node.css('top', list_top);
                    }
                }
                if (_this.options.line_text && init) {
                    var anchor_node = item_node.children('a:first');
                    var anchor_padding_left = parseInt(anchor_node.css('padding-left'));
                    var list_padding_left = parseInt(list_node.css('padding-left'));
                    var list_anchor_node = list_node.find('a:first');
                    var list_anchor_padding_left = parseInt(list_anchor_node.css('padding-left'));
                    var list_margin_left = anchor_padding_left - (list_padding_left + list_anchor_padding_left);
                    list_node.css('margin-left', list_margin_left);
                }
            });
        }
    },
    setEvents: function() {
        var _this = this;
        this.window_node.on('resize', function() {
            $('.menu .handler.active').trigger('click');
        });
        if (!this.options.mobile && this.options.init) {
            this.window_node.on('resize', function() {
                if (_this.page_node.is(':visible')) {
                    _this.setMobile();
                }
            });
        }
        if (this.options.bottom) {
            this.window_node.on('resize', function() {
                _this.setSubPosition();
            });
        }
        this.handler_node.on('click', function(event, handler_node) {
            event.stopPropagation();
            event.preventDefault();
            if (!_this.hold) {
                _this.hold = true;
                if (!_this.sticky_node.length) {
                    _this.sticky_node = $('#sticky');
                }
                if (_this.listener_node.hasClass('active')) {
                    if (_this.options.aside) {
                        _this.viewport_node.add(_this.navigation_node).add(_this.sticky_node).css('transform', '');
                    } else {
                        var inner_top = _this.inner_node.offset().top;
                        var outer_top = _this.outer_node.offset().top;
                        if (_this.scrollbar_width) {
                            _this.body_node.css('overflow-y', '');
                            _this.navigation_node.add(_this.sticky_node).css('right', _this.scrollbar_width);
                            _this.viewport_node.css('overflow', '');
                            _this.listener_node.css('right', _this.scrollbar_width);
                        }
                        _this.listener_node.css('max-height', '');
                        if (inner_top < outer_top) {
                            var listener_height = _this.listener_node.height();
                            listener_height += inner_top - outer_top;
                            _this.listener_node.addClass('notransition').height(listener_height).height();
                            _this.listener_node.removeClass('notransition');
                        }
                        _this.inner_node.css('bottom', '');
                        _this.outer_node.css('width', '');
                    }
                    _this.listener_node.removeClass('active');
                } else {
                    var active_handler_node = $('.menu .handler.active');
                    if (active_handler_node.length) {
                        active_handler_node.trigger('click', [_this.handler_node]);
                        _this.hold = false;
                        return;
                    }
                    _this.body_node.addClass('active-menu');
                    _this.viewport_node.before(_this.listener_node);
                    if (!_this.scrollbar_width) {
                        var viewport_width = _this.getWidth(_this.viewport_node);
                        var page_width = _this.page_node.outerWidth();
                        _this.scrollbar_width = viewport_width - page_width;
                    }
                    if (!_this.listener_width) {
                        _this.listener_width = _this.listener_node.outerWidth();
                    }
                    if (_this.options.aside) {
                        var outer_width = _this.getWidth(_this.outer_node);
                        var inner_width = _this.inner_node.outerWidth();
                        var scrollbar_width = outer_width - inner_width;
                        var translate_x = _this.position == 'left' ? _this.listener_width : -_this.listener_width;
                        outer_width += scrollbar_width;
                        if (_this.scrollbar_width) {
                            var scroll_top = _this.viewport_node.scrollTop();
                            _this.body_node.css('overflow-y', 'scroll');
                            _this.navigation_node.add(_this.sticky_node).css('right', 0);
                            _this.viewport_node.data('ignore_event', true).css('overflow', 'hidden').scrollTop(scroll_top);
                        }
                        _this.outer_node.width(outer_width);
                        _this.viewport_node.add(_this.navigation_node).add(_this.sticky_node).css('transform', 'translateX(' + translate_x + 'px)');
                    } else {
                        var block_height = _this.block_node.outerHeight();
                        var block_top = _this.block_node.offset().top - _this.viewport_top;
                        var block_bottom = block_height + block_top;
                        var inner_height = _this.inner_node.outerHeight();
                        if (_this.scrollbar_width) {
                            _this.listener_node.css('right', _this.scrollbar_width);
                        }
                        _this.listener_node.css({
                            height: inner_height,
                            top: block_bottom
                        });
                    }
                    _this.handler_node.addClass('active');
                    _this.listener_node.addClass('active');
                }
                if (_this.transition) {
                    _this.viewport_node.add(_this.listener_node).off('transitionend').on('transitionend', function() {
                        _this.transitionEnd(handler_node);
                        if (_this.set_mobile) {
                            _this.setMobile(true);
                        }
                        _this.set_mobile = false;
                    });
                } else {
                    _this.transitionEnd(handler_node);
                }
            }
        });
        this.viewport_node.on('click', function() {
            if (_this.listener_node.hasClass('active')) {
                _this.handler_node.trigger('click');
            }
        });
        this.item_nodes.each(function() {
            var item_node = $(this);
            var anchor_node = item_node.children('a:first');
            var list_node = item_node.children('ul:first');
            var prevent = false;
            item_node.on({
                mouseenter: function() {
                    item_node.addClass('enter');
                },
                mouseleave: function() {
                    item_node.removeClass('enter');
                }
            });
            anchor_node.on('click', function(event) {
                if (prevent) {
                    event.preventDefault();
                }
            }).each(function() {
                this.addEventListener('touchstart', function() {
                    prevent = list_node.is(':hidden');
                }, {
                    passive: true
                });
            });
        });
        this.item_nodes.on({
            mouseenter: function() {
                var item_node = $(this);
                var list_node = item_node.children('ul:first');
                var spacer_node = item_node.children('.spacer:first');
                var list_left = list_node.offset().left;
                var list_width = list_node.outerWidth();
                var list_right = list_left + list_width;
                var viewport_right = _this.viewport_node.offset().left + _this.viewport_node.width();
                if (list_right > viewport_right) {
                    if (parseInt(list_node.css('left'))) {
                        if (list_left >= list_width) {
                            list_node.css({
                                left: 'auto',
                                'margin-left': 0,
                                'margin-right': list_node.css('margin-left'),
                                'right': '100%'
                            });
                            spacer_node.css({
                                left: 'auto',
                                right: '100%'
                            });
                        }
                    } else {
                        list_node.css('left', viewport_right - list_right);
                    }
                }
            },
            mouseleave: function() {
                var item_node = $(this);
                var list_node = item_node.children('ul:first');
                var spacer_node = item_node.children('.spacer:first');
                list_node.css({
                    left: '',
                    'margin-left': '',
                    'margin-right': '',
                    right: ''
                });
                spacer_node.css({
                    left: '',
                    right: ''
                });
            }
        });
        this.anchor_nodes.on('click', function() {
            if (_this.mobile) {
                _this.handler_node.trigger('click');
            }
        });
    },
    transitionEnd: function(handler_node) {
        if (this.listener_node.hasClass('active')) {
            if (!this.options.aside) {
                var window_height = this.window_node.height();
                var listener_height = this.listener_node.outerHeight();
                var block_height = this.block_node.outerHeight();
                var block_top = this.block_node.offset().top - this.viewport_top;
                var block_bottom = block_height + block_top;
                if (this.scrollbar_width) {
                    this.body_node.css('overflow-y', 'scroll');
                    this.navigation_node.add(this.sticky_node).css('right', 0);
                    this.viewport_node.css('overflow', 'hidden');
                    this.listener_node.css('right', '');
                }
                if (listener_height > (window_height - block_bottom)) {
                    listener_height = window_height - block_bottom;
                    this.inner_node.css('bottom', 'auto');
                    this.listener_node.css('max-height', listener_height);
                    var outer_width = this.outer_node.outerWidth();
                    var inner_width = this.inner_node.outerWidth();
                    var scrollbar_width = outer_width - inner_width;
                    outer_width += scrollbar_width;
                    this.outer_node.width(outer_width);
                }
            }
        } else {
            this.handler_node.removeClass('active').after(this.listener_node);
            this.body_node.removeClass('active-menu');
            if (this.options.aside) {
                if (this.scrollbar_width) {
                    this.body_node.css('overflow-y', '');
                    this.navigation_node.add(this.sticky_node).css('right', this.scrollbar_width);
                    this.viewport_node.css('overflow', '').removeData('ignore_event');
                }
                this.outer_node.css('width', '');
            } else {
                if (this.scrollbar_width) {
                    this.listener_node.css('right', '');
                }
                this.listener_node.css({
                    position: '',
                    top: '',
                    width: ''
                });
            }
        }
        this.hold = false;
        if (handler_node) {
            handler_node.trigger('click');
        }
    },
    getHeight: function(node) {
        var height = 0;
        if (node) {
            node.addClass('measure noheight notransition');
            height = node.outerHeight();
            node.removeClass('measure noheight notransition');
        }
        return height;
    },
    getWidth: function(node) {
        var width = 0;
        if (node) {
            node.addClass('measure');
            width = node.outerWidth();
            node.removeClass('measure');
        }
        return width;
    }
};
var Parallax = function(options) {
    this.init(options);
};
Parallax.prototype = {
    init: function(options) {
        this.options = {
            selector: '[data-parallax]',
            image_selector: null,
            delay: 0.5
        };
        $.extend(this.options, options);
        this.window = $(window);
        this.viewport_node = $('#viewport');
        this.image_nodes = $();
        this.img_node = $();
        this.viewport_height = 0;
        this.parallax_top = 0;
        this.image_top = 0;
        this.start();
    },
    start: function() {
        var _this = this;
        var selectors = this.options.selector.split(',');
        this.viewport_height = this.viewport_node.height();
        if (selectors.length > 1) {
            for (var x in selectors) {
                var options = $.extend({}, this.options);
                options.selector = $.trim(selectors[x]);
                new Parallax(options);
            }
        } else {
            var parallax_nodes = $(this.options.selector);
            parallax_nodes.each(function(index) {
                if (index) {
                    var options = $.extend({}, _this.options);
                    options.selector += ':eq(' + index + ')';
                    options.init = false;
                    new Parallax(options);
                } else {
                    var image_selector = '> .parallax:first';
                    var path = null;
                    if (_this.options.image_selector) {
                        image_selector += ', ' + _this.options.image_selector;
                    }
                    _this.parallax_node = $(this);
                    _this.image_nodes = _this.parallax_node.find(image_selector);
                    _this.parallax_top = _this.parallax_node.position().top;
                    if (!_this.image_nodes.length) {
                        var background_image = _this.parallax_node.css('background-image');
                        if (background_image !== 'none') {
                            path = background_image.match(/^url\(['"]?(.*?)['"]?\)$/i);
                            if (path) {
                                path = path[1];
                            }
                        }
                    }
                    _this.setImage(path);
                    _this.setEvents();
                }
            });
        }
    },
    setImage: function(path) {
        var parallax_top = this.parallax_top;
        if (parallax_top > this.viewport_height) {
            parallax_top = this.viewport_height;
        }
        this.image_top = parallax_top * this.options.delay;
        if (path) {
            this.image_nodes = $('<div>', {
                class: 'parallax'
            });
            this.img_node = $('<img>', {
                src: path
            });
            this.image_nodes.append(this.img_node);
            this.parallax_node.prepend(this.image_nodes).css('background-image', 'none');
        } else if (this.image_nodes.hasClass('parallax')) {
            this.img_node = this.image_nodes.children('img:visible:first');
        }
        this.image_nodes.css('top', -this.image_top);
        if (this.image_nodes.hasClass('parallax')) {
            this.scaleImage();
        }
    },
    scaleImage: function() {
        this.img_node.css({
            height: '',
            width: ''
        });
        var _this = this;
        var img_height = this.img_node.height();
        if (img_height) {
            var image_height = this.image_nodes.height();
            if (img_height < image_height) {
                this.img_node.css({
                    height: '101%',
                    width: 'auto'
                });
            }
        } else {
            this.img_node.on('load', function() {
                _this.scaleImage();
            });
        }
    },
    setEvents: function() {
        var _this = this;
        this.window.on('resize', function() {
            _this.viewport_height = _this.viewport_node.height();
            _this.parallax_top = _this.parallax_node.position().top;
            _this.setImage();
            _this.viewport_node.trigger('scroll');
        });
        this.viewport_node.on('scroll', function() {
            var offset = _this.viewport_node.scrollTop();
            if (_this.parallax_top > _this.viewport_height) {
                offset -= _this.parallax_top - _this.viewport_height;
            }
            var image_top = offset * _this.options.delay;
            if (image_top < 0) {
                image_top = 0;
            } else if (_this.parallax_node.css('position') == 'fixed' && image_top > _this.image_top) {
                image_top = _this.image_top;
            }
            _this.image_nodes.css('transform', 'translateY(' + image_top + 'px)');
        });
    },
};
var Sticky = function(options) {
    this.init(options);
};
Sticky.prototype = {
    init: function(options) {
        this.options = {
            selector: '[data-sticky]'
        };
        $.extend(this.options, options);
        this.window = $(window);
        this.sticky_node = $('#sticky');
        this.sticky_page_node = $();
        this.viewport_node = $('#viewport');
        this.page_node = $();
        this.sticky_nodes = $();
        this.ghost_nodes = $();
        this.page_top = 0;
        this.scrollbar_width = 0;
        this.sticky_heights = [];
        this.sticky_tops = [];
        this.screen_widths = {
            xs: [0, 479],
            sm: [480, 767],
            md: [768, 1023],
            lg: [1024, 1279]
        };
        this.start();
    },
    start: function() {
        this.sticky_nodes = $(this.options.selector);
        if (this.sticky_nodes.length) {
            if (!this.sticky_node.length) {
                this.sticky_node = $('<div>', {
                    id: 'sticky'
                });
                this.viewport_node.before(this.sticky_node);
            }
            this.page_node = this.sticky_nodes.first().parents('.page:first');
            this.page_top = parseInt(this.page_node.css('top'));
            this.sticky_page_node = $('<div>', {
                class: this.page_node.attr('class'),
                'data-page_id': this.page_node.attr('id')
            });
            if (this.page_top) {
                this.sticky_page_node.css('top', this.page_top);
            }
            this.sticky_node.append(this.sticky_page_node);
            if (this.page_node.is(':visible')) {
                this.setStickyData();
                this.setSticky(true);
                this.setScrollSize();
                this.setStickySize();
                this.setEvents();
            }
        }
    },
    setEvents: function() {
        var _this = this;
        this.window.on('resize', function() {
            if (_this.page_node.is(':visible')) {
                _this.setStickyData();
                _this.setSticky(true);
                _this.setScrollSize();
                _this.setStickySize();
            }
        });
        this.viewport_node.on('scroll', function() {
            if (!_this.viewport_node.data('ignore_event')) {
                _this.setSticky();
                _this.setStickySize();
            }
        });
    },
    setStickyData: function() {
        var _this = this;
        this.sticky_heights = [];
        this.sticky_tops = [];
        this.sticky_nodes.filter(':visible').each(function(index) {
            var sticky_node = $(this);
            var is_sticky = sticky_node.parents('#sticky').length;
            if (is_sticky) {
                _this.ghost_nodes.eq(index).css('height', '').before(sticky_node);
            }
            var sticky_height = sticky_node.outerHeight(true);
            var sticky_top = sticky_node.position().top;
            _this.sticky_heights.push(sticky_height);
            _this.sticky_tops.push(sticky_top);
        });
    },
    setSticky: function(init) {
        var _this = this;
        var viewport_width = this.viewport_node.width();
        var scroll_top = this.viewport_node.scrollTop();
        var sticky_nodes = this.sticky_nodes.filter(':visible');
        var screen_size = 'xl';
        for (var _screen_size in this.screen_widths) {
            var min_width = this.screen_widths[_screen_size][0];
            var max_width = this.screen_widths[_screen_size][1];
            if (viewport_width >= min_width && viewport_width <= max_width) {
                screen_size = _screen_size;
            }
        }
        sticky_nodes.each(function(index) {
            var sticky_node = $(this);
            var screen_sizes = sticky_node.data('sticky').split('|');
            if (screen_sizes.indexOf('sticky') !== -1 || screen_sizes.indexOf(screen_size) !== -1) {
                var sticky_top = _this.sticky_tops[index];
                var prev_sticky_nodes = sticky_nodes.filter(':lt(' + index + ')');
                var prev_sticky_heights = 0;
                prev_sticky_nodes.each(function() {
                    prev_sticky_heights += $(this).outerHeight();
                });
                if (init || sticky_top !== prev_sticky_heights) {
                    var is_sticky = sticky_node.parents('#sticky').length;
                    var ghost_node = _this.ghost_nodes.eq(index);
                    sticky_top -= prev_sticky_heights;
                    if (_this.page_top) {
                        sticky_top -= _this.page_top;
                    }
                    if (!is_sticky && sticky_top <= scroll_top) {
                        var sticky_height = _this.sticky_heights[index];
                        if (!ghost_node.length) {
                            ghost_node = $('<div>', {
                                class: 'ghost'
                            });
                            sticky_node.after(ghost_node);
                            _this.ghost_nodes = _this.ghost_nodes.add(ghost_node);
                        }
                        _this.sticky_page_node.append(sticky_node);
                        ghost_node.css('height', sticky_height);
                    } else if (is_sticky && sticky_top > scroll_top) {
                        ghost_node.css('height', '').before(sticky_node);
                    }
                }
            } else {
                var is_sticky = sticky_node.parents('#sticky').length;
                if (is_sticky) {
                    var ghost_node = _this.ghost_nodes.eq(index);
                    ghost_node.css('height', '').before(sticky_node);
                }
            }
        });
    },
    setScrollSize: function() {
        this.viewport_node.css('overflow', 'hidden');
        var viewport_width = this.viewport_node.outerWidth();
        this.viewport_node.css('overflow', '');
        this.scrollbar_width = viewport_width - this.page_node.outerWidth();
    },
    setStickySize: function() {
        var right = this.scrollbar_width ? this.scrollbar_width : '';
        this.sticky_node.css('right', right);
    }
};
var Equalize = function(options) {
    this.init(options);
};
Equalize.prototype = {
    init: function(options) {
        this.options = {
            selector: '.equalize'
        };
        $.extend(this.options, options);
        this.window = $(window);
        this.equalize_node = $();
        this.size = null;
        this.sizes = ['xs', 'sm', 'md', 'lg', 'xl'];
        this.start();
    },
    start: function() {
        var _this = this;
        var selectors = this.options.selector.split(',');
        if (selectors.length > 1) {
            for (var x in selectors) {
                var options = $.extend({}, this.options);
                options.selector = $.trim(selectors[x]);
                new Equalize(options);
            }
        } else {
            var equalize_nodes = $(this.options.selector);
            equalize_nodes.each(function(index) {
                if (index) {
                    var options = $.extend({}, _this.options);
                    options.selector += ':eq(' + index + ')';
                    new Equalize(options);
                } else {
                    _this.equalize_node = $(this);
                    _this.column_nodes = _this.equalize_node.children();
                    var img_nodes = _this.equalize_node.find('img');
                    var img_count = img_nodes.length;
                    if (img_count) {
                        var counter = 0;
                        img_nodes.each(function() {
                            var img_node = $(this);
                            var img_height = img_node.height();
                            if (img_height) {
                                counter++;
                                if (counter == img_count) {
                                    _this.setSize();
                                    _this.equalize();
                                }
                            } else {
                                img_node.on('load', function() {
                                    counter++;
                                    if (counter == img_count) {
                                        _this.setSize();
                                        _this.equalize();
                                    }
                                });
                            }
                        });
                    } else {
                        _this.setSize();
                        _this.equalize();
                    }
                    _this.setEvents();
                }
            });
        }
    },
    setSize: function() {
        var window_width = this.window.width();
        var size = null;
        switch (true) {
            case (window_width < 480):
                size = this.sizes[0];
                break;
            case (window_width < 768):
                size = this.sizes[1];
                break;
            case (window_width < 1024):
                size = this.sizes[2];
                break;
            case (window_width < 1280):
                size = this.sizes[3];
                break;
            default:
                size = this.sizes[4];
        }
        if (size != this.size) {
            this.size = size;
            return true;
        }
        return false;
    },
    setEvents: function() {
        var _this = this;
        this.window.on('resize', function(event, force) {
            if (force || _this.setSize()) {
                _this.equalize();
            }
        });
    },
    equalize: function() {
        var _this = this;
        var equalize_width = this.equalize_node.width();
        var column_count = this.column_nodes.length;
        var columns_width = 0;
        var prev_elements = [];
        var prev_heights = [];
        var ignore = false;
        this.column_nodes.css('clear', '').each(function(index) {
            var column_node = $(this);
            var column_width = column_node.outerWidth(true);
            var box_node = column_node.children('.box, [class^="box-"]').first();
            var element_nodes = box_node.length ? box_node.children(':not(:last)') : column_node.children(':not(:last)');
            columns_width += column_width;
            if (columns_width > equalize_width) {
                _this.setMargins(prev_elements, prev_heights);
                columns_width = column_width;
                prev_elements = [];
                prev_heights = [];
                ignore = false;
                column_node.css('clear', 'both');
            }
            if (!ignore) {
                element_nodes.css('margin-bottom', '').each(function(_index) {
                    var element_node = $(this);
                    if (prev_elements[_index]) {
                        var element_class = element_node.attr('class');
                        var prev_element_count = prev_elements[_index].length;
                        var prev_element_node = prev_elements[_index][prev_element_count - 1][0];
                        var prev_element_class = prev_element_node.attr('class');
                        if (element_class && prev_element_class) {
                            var first_element_class = element_class.match(/^[\w\-]+/);
                            var first_prev_element_class = prev_element_class.match(/^[\w\-]+/);
                            if (first_element_class) {
                                element_class = first_element_class[0];
                            }
                            if (first_prev_element_class) {
                                prev_element_class = first_prev_element_class[0];
                            }
                        }
                        if (element_class != prev_element_class) {
                            ignore = true;
                            return false;
                        }
                    }
                    var element_height = element_node.outerHeight(true);
                    var element_margin = parseInt(element_node.css('margin-bottom'));
                    if (!prev_elements[_index]) {
                        prev_elements[_index] = [];
                    }
                    if (!prev_heights[_index]) {
                        prev_heights[_index] = [];
                    }
                    prev_elements[_index].push([element_node, element_height, element_margin]);
                    prev_heights[_index].push(element_height);
                });
                if (ignore || index == (column_count - 1)) {
                    _this.setMargins(prev_elements, prev_heights);
                }
            }
        });
    },
    setMargins: function(elements, heights) {
        if (elements && heights) {
            for (var x in elements) {
                if (heights[x]) {
                    var equal_elements = elements[x];
                    var height = Math.max.apply(Math, heights[x]);
                    for (var y in equal_elements) {
                        var element = equal_elements[y];
                        if (element && element.length > 2) {
                            var element_node = element[0];
                            var element_height = element[1];
                            var element_margin = element[2];
                            if (element_height < height) {
                                var margin = (height - element_height) + element_margin;
                                element_node.css('margin-bottom', margin);
                            }
                        }
                    }
                }
            }
        }
    }
};
var Share = function(options) {
    this.init(options);
};
Share.prototype = {
    init: function(options) {
        this.options = {
            selector: '.share'
        };
        $.extend(this.options, options);
        this.share_node = $();
        this.start();
    },
    start: function() {
        var _this = this;
        var selectors = this.options.selector.split(',');
        if (selectors.length > 1) {
            for (var x in selectors) {
                var options = $.extend({}, this.options);
                options.selector = $.trim(selectors[x]);
                new Share(options);
            }
        } else {
            var share_nodes = $(this.options.selector);
            share_nodes.each(function(index) {
                if (index) {
                    var options = $.extend({}, _this.options);
                    options.selector += ':eq(' + index + ')';
                    new Share(options);
                } else {
                    _this.share_node = $(this);
                    _this.setEvents();
                }
            });
        }
    },
    setEvents: function() {
        if (this.share_node.length) {
            var share_nodes = this.share_node.children('[class*=share-]');
            share_nodes.each(function() {
                var share_node = $(this);
                var share_class = share_node.attr('class');
                var match = null;
                if (match = share_class.match(/(?:^|\s)share-([a-z0-9\-]+)/i)) {
                    var share_type = match[1];
                    share_node.on('click', function() {
                        var title_node = $('title:first');
                        var title = title_node.length ? encodeURIComponent(title_node.text()) : '';
                        var url = encodeURIComponent(location.href);
                        var href = '#';
                        var target = '_blank';
                        switch (share_type) {
                            case 'email':
                                href = 'mailto:?subject=' + title + '&body=' + url;
                                target = '';
                                break;
                            case 'facebook':
                                href = 'https://www.facebook.com/sharer/sharer.php?u=' + url;
                                break;
                            case 'twitter':
                                href = 'https://twitter.com/intent/tweet?text=' + url;
                                break;
                            case 'linkedin':
                                href = 'https://www.linkedin.com/shareArticle?mini=true&url=' + url + '&title=' + title;
                                break;
                            case 'pinterest':
                                href = 'https://pinterest.com/pin/create/button/?url=&media=' + url;
                        }
                        share_node.attr({
                            href: href,
                            target: target
                        });
                    });
                }
            });
        }
    }
};
var Scale = function(options) {
    this.init(options);
};
Scale.prototype = {
    init: function(options) {
        this.options = {
            selector: '[class*="heading-"], [class*="text-"]',
            min_font_size: 8
        };
        $.extend(this.options, options);
        this.window = $(window);
        this.scale_nodes = $();
        this.start();
    },
    start: function() {
        this.scale_nodes = $(this.options.selector).not(':has([style*="font-size"])');
        this.setEvents();
        this.scale();
    },
    setEvents: function() {
        var _this = this;
        this.window.on('resize', function() {
            _this.scale();
        });
    },
    scale: function(scale_nodes, font_size) {
        var _this = this;
        if (!scale_nodes) {
            scale_nodes = this.scale_nodes;
        }
        scale_nodes.each(function() {
            var scale_node = $(this);
            var parent_node = scale_node.parent(':visible');
            var parent_width = Math.ceil(parent_node.width());
            scale_node.css('font-size', '').wrapInner('<span>');
            parent_node.css('max-width', '100%');
            var span_node = scale_node.children('span:first');
            var _parent_width = Math.ceil(parent_node.width());
            if (_parent_width < parent_width) {
                parent_width = _parent_width;
                var span_width = Math.floor(span_node.outerWidth());
                parent_node.css('max-width', '');
            } else {
                parent_node.css('max-width', '');
                var span_width = Math.floor(span_node.outerWidth());
            }
            span_node.contents().unwrap();
            if (span_width > parent_width) {
                var _font_size = font_size ? font_size : parseInt(scale_node.css('font-size'));
                var reduction = Math.round(_font_size / 10);
                if (reduction < 1) {
                    reduction = 1;
                }
                _font_size -= reduction;
                if (_font_size >= _this.options.min_font_size) {
                    scale_node.css('font-size', _font_size).wrapInner('<span>');
                    span_node = scale_node.children('span:first');
                    span_width = Math.floor(span_node.outerWidth());
                    span_node.contents().unwrap();
                    if (_font_size > _this.options.min_font_size && span_width > parent_width) {
                        _this.scale(scale_node, _font_size);
                    }
                }
            }
        });
    }
};
var Effect = function(options) {
    this.init(options);
};
Effect.prototype = {
    init: function(options) {
        this.options = {
            selector: '[data-effect]:not(.effect-init)'
        };
        $.extend(this.options, options);
        this.window = $(window);
        this.viewport_node = $('#viewport');
        this.effect_nodes = $();
        this.viewport_height = 0;
        this.start();
    },
    start: function() {
        this.effect_nodes = $(this.options.selector);
        this.viewport_height = this.viewport_node.height();
        this.setEvents();
        this.setEffects();
    },
    setEvents: function() {
        var _this = this;
        this.effect_nodes.each(function() {
            var effect_node = $(this);
            var transforms = effect_node.css('transform').split(', ');
            var translate_y = 0;
            if (transforms[5]) {
                translate_y = parseFloat(transforms[5]);
                var scale = parseFloat(transforms[3]);
                if (scale < 1) {
                    var height = effect_node.outerHeight();
                    translate_y += (height - (height * scale)) / 2;
                } else if (scale > 1) {
                    var height = effect_node.outerHeight();
                    translate_y -= (height * scale - height) / 2;
                }
            }
            effect_node.addClass('effect-init').data('translate_y', translate_y).on('start', function(event, callback) {
                if (effect_node.hasClass('effect-end')) {
                    effect_node.addClass('notransition').removeClass('effect-end').height();
                    effect_node.removeClass('notransition').off('transitionend');
                }
                if (!effect_node.hasClass('effect-play')) {
                    effect_node.addClass('effect-start effect-play').on('transitionend', function() {
                        effect_node.off('transitionend').addClass('effect-end').removeClass('effect-start effect-play');
                        if (callback) {
                            callback();
                        }
                    });
                }
            });
            if (effect_node.data('effect_revert') !== undefined) {
                effect_node.on('revert', function() {
                    if (!effect_node.hasClass('effect-play')) {
                        effect_node.addClass('effect-start effect-play').removeClass('effect-end').on('transitionend', function() {
                            effect_node.off('transitionend').removeClass('effect-play');
                        }).removeClass('effect-start');
                    }
                });
            }
        });
        this.viewport_node.on('scroll', function() {
            _this.setEffects();
        });
        this.window.on('resize', function() {
            _this.viewport_height = _this.viewport_node.height();
            _this.setEffects();
        });
    },
    setEffects: function() {
        var _this = this;
        this.effect_nodes.filter(':not(.effect-play)').each(function() {
            var effect_node = $(this);
            if (!effect_node.hasClass('effect-end')) {
                var effect_top = Math.round(effect_node.offset().top - effect_node.data('translate_y'));
                var effect_start = (effect_top <= (_this.viewport_height / 2));
                if (!effect_start) {
                    var effect_bottom = effect_top + effect_node.outerHeight();
                    effect_start = (effect_bottom <= _this.viewport_height);
                    if (!effect_start) {
                        var page_height = effect_node.parents('.page:first').outerHeight(true);
                        if (_this.viewport_node.scrollTop() + _this.viewport_height >= Math.floor(page_height)) {
                            effect_start = true;
                        }
                    }
                }
                if (effect_start) {
                    effect_node.trigger('start');
                }
            } else if (effect_node.data('effect_revert') !== undefined) {
                var effect_top = effect_node.offset().top;
                var effect_bottom = effect_top + effect_node.outerHeight();
                var effect_revert = (effect_top > (_this.viewport_height / 2) && effect_bottom > _this.viewport_height);
                if (!effect_revert) {
                    var page_height = effect_node.parents('.page:first').outerHeight(true);
                    if (_this.viewport_node.scrollTop() + _this.viewport_height >= Math.floor(page_height)) {
                        revert_start = true;
                    }
                }
                if (effect_revert) {
                    effect_node.trigger('revert');
                }
            }
        });
    }
};
var Video = function(options) {
    this.init(options);
};
Video.prototype = {
    init: function(options) {
        this.options = {
            selector: 'video, iframe[allowfullscreen]'
        };
        $.extend(this.options, options);
        this.window = $(window);
        this.video_node = $();
        this.parent_node = $();
        this.play_node = $();
        this.is_video = false;
        this.is_iframe = false;
        this.parent_ratio = undefined;
        this.start();
    },
    start: function() {
        var _this = this;
        var selectors = this.options.selector.split(',');
        if (selectors.length > 1) {
            for (var x in selectors) {
                var options = $.extend({}, this.options);
                options.selector = $.trim(selectors[x]);
                new Video(options);
            }
        } else {
            var video_nodes = $(this.options.selector);
            video_nodes.each(function(index) {
                if (index) {
                    var options = $.extend({}, _this.options);
                    options.selector += ':eq(' + index + ')';
                    new Video(options);
                } else {
                    _this.video_node = $(this);
                    _this.is_video = _this.video_node.is('video');
                    _this.is_iframe = _this.video_node.is('iframe');
                    _this.parent_node = _this.video_node.parent();
                    var has_image = _this.parent_node.children('img:visible:first').length ? true : false;
                    var show_video = has_image ? false : true;
                    var has_controls = false;
                    if (_this.is_video) {
                        if (!show_video && _this.video_node.attr('autoplay') !== undefined) {
                            show_video = true;
                        }
                        if (!show_video || _this.video_node.attr('controls') !== undefined) {
                            has_controls = true;
                        }
                    } else if (_this.is_iframe) {
                        var video_src = _this.video_node.attr('src');
                        var parent_ratio = _this.parent_node.data('ratio');
                        var has_contain = (_this.parent_node.data('contain') !== undefined);
                        var has_ratio = (parent_ratio !== undefined);
                        var is_absolute = (_this.parent_node.css('position') == 'absolute');
                        if (!show_video && video_src && video_src.match(/[?&]autoplay=1/i)) {
                            show_video = true;
                        }
                        if (!show_video || (video_src && !video_src.match(/[?&]controls=0/i))) {
                            has_controls = true;
                        }
                        if (!has_contain) {
                            if (has_ratio && !has_image) {
                                _this.scaleIframe(parent_ratio);
                            } else if (is_absolute) {
                                _this.scaleIframe();
                                _this.window.on('resize', function() {
                                    _this.scaleIframe();
                                });
                            }
                        }
                        if (!has_ratio && !is_absolute) {
                            _this.parent_node.addClass('video-16x9');
                        }
                    }
                    if (show_video) {
                        _this.video_node.css('visibility', 'visible');
                        _this.parent_node.addClass('playing');
                    } else if (has_image) {
                        _this.play_node = $('<a>', {
                            href: '#',
                            class: 'play'
                        });
                        _this.parent_node.append(_this.play_node);
                    }
                    if (has_controls) {
                        _this.parent_node.addClass('has-controls');
                    }
                    _this.setEvents();
                }
            });
        }
    },
    scaleIframe: function(ratio) {
        var parent_height = this.parent_node.height();
        var parent_width = this.parent_node.width();
        ratio = ratio ? ratio.split('x') : [parent_width, parent_height];
        var parent_multiplier = ratio[0] / ratio[1];
        var video_multiplier = 16 / 9;
        var multiplier = null;
        if (parent_multiplier > video_multiplier) {
            var video_width = parent_height * video_multiplier;
            multiplier = parent_width / video_width;
        } else if (parent_multiplier < video_multiplier) {
            var video_height = parent_width / video_multiplier;
            multiplier = parent_height / video_height;
        }
        if (multiplier) {
            var scale = multiplier * 100 + 1 + '%';
            this.video_node.css({
                height: scale,
                width: scale
            });
        }
    },
    setEvents: function() {
        var _this = this;
        this.play_node.on('click', function(event) {
            event.preventDefault();
            _this.parent_node.addClass('playing');
            if (_this.is_video) {
                _this.video_node.get(0).play();
            } else if (_this.is_iframe) {
                var video_src = _this.video_node.attr('src');
                if (video_src) {
                    var separator = video_src.indexOf('?') > -1 ? '&' : '?';
                    video_src += separator + 'autoplay=1';
                    _this.video_node.attr('src', video_src);
                }
            }
        });
    }
};
const Form = function(options) {
    this.init(options);
};
Form.prototype = {
    init: function(options) {
        this.options = {
            selector: 'form[data-form_type]'
        };
        $.extend(this.options, options);
        this.window_node = $(window);
        this.document_node = $(document);
        this.viewport_node = $('#viewport');
        this.page_node = $();
        this.form_node = $();
        this.recaptcha_nodes = $('.g-recaptcha');
        this.hold = false;
        this.recaptcha_index = 0;
        this.recaptcha_type = null;
        this.start();
    },
    start: function() {
        let _this = this;
        let selectors = this.options.selector.split(',');
        if (selectors.length > 1) {
            for (let x in selectors) {
                let options = $.extend({}, this.options);
                options.selector = $.trim(selectors[x]);
                new Form(options);
            }
        } else {
            let form_nodes = $(this.options.selector);
            form_nodes.each(function(index) {
                if (index) {
                    let options = $.extend({}, _this.options);
                    options.selector += ':eq(' + index + ')';
                    new Form(options);
                } else {
                    _this.form_node = $(this);
                    _this.page_node = _this.form_node.parents('.page:first');
                    let recaptcha_node = _this.form_node.children('.g-recaptcha:first');
                    _this.recaptcha_index = _this.recaptcha_nodes.index(recaptcha_node);
                    _this.recaptcha_type = recaptcha_node.data('type');
                    switch (_this.recaptcha_type) {
                        case 'v3':
                        case 'v2_invisible':
                            _this.setRecaptchaEvents();
                            break;
                        case 'v2_checkbox':
                            _this.loadRecaptchaApi();
                    }
                    switch (_this.form_node.data('form_type')) {
                        case 'search':
                            _this.setSearchEvents();
                            break;
                        default:
                            _this.setEvents();
                    }
                }
            });
        }
    },
    loadRecaptchaApi: function() {
        if (!$('#recaptcha-api').length) {
            $('head').append('<script id="recaptcha-api" src="https://www.google.com/recaptcha/api.js"></script>');
        }
    },
    setEvents: function() {
        let _this = this;
        this.setSelectEvents();
        this.setRadioEvents();
        this.setCheckboxEvents();
        this.setDateEvents();
        this.setFileEvents();
        this.form_node.find('input[readonly]').on('focus', function() {
            this.blur();
        });
        this.form_node.find('input').on('keydown', function(event) {
            if (event.key == 'Enter') {
                _this.form_node.trigger('submit');
            }
        });
        this.form_node.children('a:last').on('click', function(event) {
            event.preventDefault();
            _this.form_node.trigger('submit');
        });
        this.form_node.on('submit', function(event, recaptcha_response) {
            event.preventDefault();
            if (!_this.hold) {
                switch (_this.recaptcha_type) {
                    case 'v3':
                    case 'v2_invisible':
                        if (!recaptcha_response && typeof grecaptcha != 'undefined') {
                            return;
                        }
                }
                _this.submit();
            }
        });
    },
    setRecaptchaEvents: function() {
        let _this = this;
        this.form_node.on('submit', function() {
            _this.loadRecaptchaApi();
        });
        this.form_node.find('input').on('input change', function() {
            _this.loadRecaptchaApi();
        });
    },
    setSelectEvents: function() {
        let _this = this;
        let click_select = false;
        this.window_node.on('resize', function() {
            _this.setOptionsPosition($('.select.active'));
        });
        this.document_node.on('click', function() {
            if (!click_select) {
                $('.select.active').trigger('click');
            }
        });
        this.viewport_node.on('scroll', function() {
            _this.setOptionsPosition($('.select.active'));
        });
        this.form_node.find('.select').each(function() {
            let select_node = $(this);
            let select_id = select_node.attr('id');
            let input_node = select_node.children('input[type="text"]:first');
            let button_node = select_node.children('button:first');
            let options_node = select_node.children('.options:first');
            let option_nodes = options_node.find('.option');
            let text_border_right_width = parseInt(input_node.css('border-right-width'));
            let button_border_left_width = parseInt(button_node.css('border-left-width'));
            if (text_border_right_width && button_border_left_width) {
                input_node.css('margin-right', -text_border_right_width);
            }
            select_node.on('click', function(event) {
                event.stopPropagation();
                if (select_node.hasClass('active')) {
                    select_node.removeClass('active top');
                    options_node.removeClass('top').removeAttr('style');
                } else {
                    click_select = true;
                    setTimeout(function() {
                        click_select = false;
                    }, 1);
                    let active_select_nodes = $('.select.active');
                    active_select_nodes.trigger('click');
                    _this.showOptions(select_node);
                }
            });
            button_node.on('click', function(event) {
                event.preventDefault();
            });
            options_node.on('click', function(event) {
                event.stopPropagation();
            });
            option_nodes.on('click', function(event) {
                let option_node = $(this);
                if (option_node.hasClass('active')) {
                    let option_href = option_node.attr('href');
                    if (option_href != undefined) {
                        event.preventDefault();
                    }
                } else {
                    let placeholder = option_node.data('placeholder');
                    let option_text = '';
                    if (typeof placeholder == 'undefined') {
                        option_text = option_node.text();
                    }
                    option_nodes.removeClass('active');
                    option_node.addClass('active');
                    input_node.val(option_text).trigger('change');
                }
                select_node.trigger('click');
            });
            if (select_id) {
                let label_node = $('label[for="' + select_id + '"]');
                label_node.on('click', function() {
                    select_node.trigger('click');
                });
            }
        });
    },
    showOptions: function(select_node) {
        options_node = select_node.children('.options, .results').first();
        options_node.css('visibility', 'hidden');
        select_node.addClass('active');
        this.setOptionsPosition(select_node);
    },
    setOptionsPosition: function(select_nodes) {
        if (select_nodes && select_nodes.length) {
            let window_height = this.window_node.height();
            let window_width = this.window_node.width();
            let viewport_offset = this.viewport_node.offset();
            let viewport_height = this.viewport_node.height();
            let viewport_bottom = viewport_offset.top + viewport_height;
            viewport_offset.bottom = window_height - viewport_bottom;
            let page_offset = this.page_node.offset();
            let page_width = this.page_node.width();
            let page_right = page_offset.left + page_width;
            page_offset.right = window_width - page_right;
            select_nodes.each(function() {
                let select_node = $(this);
                let text_node = select_node.children('input[type="text"], input[type="search"]').first();
                let options_node = select_node.children('.options, .results').first();
                let select_offset = select_node.offset();
                let select_height = select_node.outerHeight();
                let select_width = select_node.outerWidth();
                let select_bottom = select_offset.top + select_height;
                let select_right = select_offset.left + select_width;
                select_offset.bottom = window_height - select_bottom;
                select_offset.right = window_width - select_right;
                let select_rel_offset = {
                    top: select_offset.top - viewport_offset.top,
                    bottom: select_offset.bottom - viewport_offset.bottom,
                    left: select_offset.left - page_offset.left,
                    right: select_offset.right - page_offset.right
                };
                options_node.css({
                    bottom: '',
                    left: 0,
                    maxWidth: '',
                    minWidth: select_width,
                    right: '',
                    top: 0,
                    visibility: 'hidden'
                });
                let options_height = options_node.outerHeight(true);
                let options_width = options_node.outerWidth(true);
                let options_bottom = select_bottom + options_height;
                let options_right = select_offset.left + options_width;
                let options_margin_top = parseInt(options_node.css('margin-top'));
                let options_margin_bottom = parseInt(options_node.css('margin-bottom'));
                let options_margin_left = parseInt(options_node.css('margin-left'));
                let options_margin_right = parseInt(options_node.css('margin-right'));
                let options_max_width = parseInt(options_node.css('max-width'));
                if (options_bottom > viewport_bottom && select_rel_offset.top > select_rel_offset.bottom) {
                    let options_max_height = select_rel_offset.top - options_margin_top - options_margin_bottom;
                    if (!options_margin_bottom) {
                        let text_border_top_width = parseInt(text_node.css('border-top-width'));
                        let options_border_bottom_width = parseInt(options_node.css('border-bottom-width'));
                        if (text_border_top_width && options_border_bottom_width) {
                            options_max_height += text_border_top_width;
                        }
                    }
                    select_node.addClass('top');
                    options_node.addClass('top').css({
                        bottom: select_offset.bottom + select_height,
                        maxHeight: options_max_height,
                        top: ''
                    });
                } else {
                    let options_max_height = select_rel_offset.bottom - options_margin_top - options_margin_bottom;
                    if (!options_margin_top) {
                        let text_border_bottom_width = parseInt(text_node.css('border-bottom-width'));
                        let options_border_top_width = parseInt(options_node.css('border-top-width'));
                        if (text_border_bottom_width && options_border_top_width) {
                            options_max_height += text_border_bottom_width;
                        }
                    }
                    select_node.removeClass('top');
                    options_node.removeClass('top').css({
                        maxHeight: options_max_height,
                        top: select_bottom
                    });
                }
                if (options_right > page_right && select_rel_offset.left > select_rel_offset.right) {
                    let _options_max_width = page_width - select_rel_offset.right - options_margin_left - options_margin_right;
                    if (isNaN(options_max_width) || options_max_width > _options_max_width) {
                        options_max_width = _options_max_width;
                    }
                    options_node.css({
                        left: '',
                        maxWidth: options_max_width,
                        right: select_offset.right - options_margin_right
                    });
                } else {
                    let _options_max_width = page_width - select_rel_offset.left - options_margin_left - options_margin_right;
                    if (isNaN(options_max_width) || options_max_width > _options_max_width) {
                        options_max_width = _options_max_width;
                    }
                    options_node.css({
                        left: select_offset.left - options_margin_left,
                        maxWidth: options_max_width,
                    });
                }
                options_node.css('visibility', '');
            });
        }
    },
    setRadioEvents: function() {
        this.form_node.find('.radio').each(function() {
            let radio_node = $(this);
            let radios_node = radio_node.parents('.radios:first');
            let input_node = radio_node.children('input:first');
            let label_node = radio_node.next('label');
            radio_node.on('click', function() {
                if (radio_node.hasClass('active')) {
                    input_node.prop('checked', false).trigger('change');
                    radio_node.removeClass('active');
                } else {
                    let active_radio_nodes = radios_node.find('.radio.active');
                    input_node.prop('checked', true).trigger('change');
                    active_radio_nodes.removeClass('active');
                    radio_node.addClass('active');
                }
            });
            label_node.on('click', function() {
                radio_node.trigger('click');
            });
        });
    },
    setCheckboxEvents: function() {
        this.form_node.find('.checkbox').each(function() {
            let checkbox_node = $(this);
            let input_node = checkbox_node.children('input:first');
            let label_node = checkbox_node.next('label');
            checkbox_node.on('click', function() {
                if (checkbox_node.hasClass('active')) {
                    input_node.prop('checked', false).trigger('change');
                    checkbox_node.removeClass('active');
                } else {
                    input_node.prop('checked', true).trigger('change');
                    checkbox_node.addClass('active');
                }
            });
            label_node.on('click', function() {
                checkbox_node.trigger('click');
            });
        });
    },
    setDateEvents: function() {
        this.form_node.find('.date, .date-time').each(function() {
            let date_node = $(this);
            let input_node = date_node.children('input:first');
            let button_node = date_node.children('button:first');
            let input_border_right_width = parseInt(input_node.css('border-right-width'));
            let button_border_left_width = parseInt(button_node.css('border-left-width'));
            if (input_border_right_width && button_border_left_width) {
                input_node.css('margin-right', -input_border_right_width);
            }
            button_node.on('click', function(event) {
                event.preventDefault();
            });
            let options = {
                wrap: true,
                dateFormat: "d-m-Y",
                onReady: function() {
                    input_node.attr('type', 'text');
                },
                onOpen: function() {
                    date_node.addClass('active');
                },
                onClose: function() {
                    date_node.removeClass('active');
                },
                onChange: function() {
                    input_node.trigger('change');
                }
            }
            if (LANGUAGE_CODE && LANGUAGE_CODE != 'en' && flatpickr.l10ns[LANGUAGE_CODE]) {
                options.locale = LANGUAGE_CODE;
            }
            if (date_node.hasClass('date-time')) {
                options.dateFormat = "d-m-Y H:i",
                    options.enableTime = true;
                options.time_24hr = true;
            }
            date_node.flatpickr(options);
        });
    },
    setFileEvents: function() {
        this.form_node.find('.file').each(function() {
            let file_node = $(this);
            let file_id = file_node.attr('id');
            let input_file_node = file_node.children('input[type="file"]:first');
            let input_text_node = file_node.children('input[type="text"]:first');
            let button_node = file_node.children('button:first');
            let text_border_right_width = parseInt(input_text_node.css('border-right-width'));
            let button_border_left_width = parseInt(button_node.css('border-left-width'));
            if (text_border_right_width && button_border_left_width) {
                input_text_node.css('margin-right', -text_border_right_width);
            }
            file_node.on('click', function() {
                input_file_node.trigger('click');
            });
            input_file_node.on({
                click: function(event) {
                    event.stopPropagation();
                },
                change: function() {
                    let value = input_file_node.val();
                    let filename = value.replace(/^.*[\/\\]/, '');
                    input_text_node.val(filename).trigger('change');
                }
            });
            button_node.on('click', function(event) {
                event.preventDefault();
            });
            if (file_id) {
                let label_node = $('label[for="' + file_id + '"]');
                label_node.on('click', function() {
                    file_node.trigger('click');
                });
            }
        });
    },
    submit: function(recaptcha_response) {
        this.hold = true;
        this.form_node.addClass('disabled');
        let _this = this;
        let form_action = this.form_node.attr('action');
        let form_data = new FormData(this.form_node.get(0));
        let field_nodes = this.form_node.children('div:not(.feedback)');
        let redirect = this.form_node.data('redirect');
        if (recaptcha_response) {
            form_data.set('g-recaptcha-response', recaptcha_response);
        }
        let options = {
            type: 'post',
            url: form_action,
            data: form_data,
            cache: false,
            contentType: false,
            processData: false
        };
        options.success = function(data) {
            if (data && data.status) {
                let feedback_node = _this.form_node.children('.feedback:first');
                feedback_node.remove();
                feedback_node = $('<div>', {
                    class: 'feedback',
                    html: data.feedback
                });
                switch (data.status) {
                    case 'error':
                        _this.form_node.find('.error').removeClass('error');
                        feedback_node.addClass('error');
                        if (_this.form_node.css('display') == 'flex' && _this.form_node.css('flex-direction') == 'row') {
                            field_nodes.first().before(feedback_node);
                        } else {
                            field_nodes.last().after(feedback_node);
                        }
                        let field_selector = 'input[type="text"]:first';
                        field_selector += ', input[type="password"]:first';
                        field_selector += ', input[type="email"]:first';
                        field_selector += ', input[type="tel"]:first';
                        field_selector += ', textarea:first';
                        field_selector += ', button:first';
                        field_selector += ', .options:first';
                        field_selector += ', .radio';
                        field_selector += ', .checkbox';
                        $.each(data.error_fields, function(index, value) {
                            field_nodes.has('[name="' + value + '"], [name^="' + value + '["]').find(field_selector).addClass('error');
                        });
                        if (_this.recaptcha_type == 'v2_invisible' && typeof grecaptcha != 'undefined' && typeof grecaptcha.reset == 'function') {
                            grecaptcha.reset(_this.recaptcha_index);
                        }
                        break;
                    case 'success':
                        if (data.redirect) {
                            location.href = data.redirect;
                        } else if (redirect) {
                            location.href = redirect;
                        } else {
                            feedback_node.addClass('success');
                            _this.form_node.replaceWith(feedback_node);
                        }
                        return;
                }
                _this.hold = false;
                _this.form_node.removeClass('disabled');
            }
        }
        ;
        $.ajax(options);
    },
    setSearchEvents: function() {
        let _this = this;
        let form_action = this.form_node.attr('action');
        let search_node = this.form_node.find('.search:first');
        let input_node = search_node.children('input:first');
        let button_node = search_node.children('button:first');
        let overlay_node = search_node.children('.overlay:first');
        let results_node = search_node.children('.results:first');
        let result_title_node = results_node.find('.result-title:first');
        let result_content_node = results_node.find('.result-content:first');
        let request = null;
        let timeout = false;
        let input_border_right_width = parseInt(input_node.css('border-right-width'));
        let button_border_left_width = parseInt(button_node.css('border-left-width'));
        if (input_border_right_width && button_border_left_width) {
            input_node.css('margin-right', -input_border_right_width);
        }
        this.window_node.on('resize', function() {
            _this.setOptionsPosition($('.search.active'));
        });
        this.viewport_node.on('scroll', function() {
            _this.setOptionsPosition($('.search.active'));
        });
        this.form_node.on('submit', function(event) {
            event.preventDefault();
            if (request) {
                request.abort();
            }
            request = $.ajax({
                type: 'get',
                url: form_action,
                data: _this.form_node.serialize(),
                success: function(data) {
                    let show_results = false;
                    results_node.empty();
                    if (data.length) {
                        $.each(data, function(key, value) {
                            if (value.url && value.title) {
                                show_results = true;
                                result_title_node.clone().attr('href', value.url).html(value.title).appendTo(results_node);
                                result_content_node.clone().html(value.content ? '<p>' + value.content + '</p>' : '').appendTo(results_node);
                            } else if (value.content) {
                                show_results = true;
                                result_content_node.clone().html('<p>' + value.content + '</p>').appendTo(results_node);
                            }
                        });
                    }
                    if (show_results) {
                        search_node.trigger('show');
                    } else {
                        search_node.trigger('hide');
                    }
                }
            });
        });
        search_node.on({
            show: function() {
                _this.showOptions(search_node);
            },
            hide: function() {
                search_node.removeClass('active');
            }
        });
        input_node.on({
            focus: function() {
                search_node.addClass('focus');
                if (input_node.val() && results_node.html()) {
                    search_node.trigger('show');
                }
            },
            blur: function() {
                search_node.removeClass('focus');
            },
            input: function() {
                if (timeout) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function() {
                    _this.form_node.trigger('submit');
                }, 250);
            }
        });
        overlay_node.on('click', function() {
            search_node.removeClass('active');
        });
    }
};
const Anchor = function(options) {
    this.init(options);
};
Anchor.prototype = {
    init: function(options) {
        this.options = {
            selector: 'a[name]'
        };
        $.extend(this.options, options);
        this.viewport_node = $('#viewport');
        this.anchor_nodes = $();
        this.anchor_nodes_reverse = $();
        this.li_nodes = $();
        this.hash = '#';
        this.hold = false;
        this.start();
    },
    start: function() {
        this.hash = location.hash;
        let _this = this;
        let anchor_names = [];
        $(this.options.selector).each(function() {
            let anchor_node = $(this);
            if (anchor_node.is(':visible')) {
                let anchor_name = anchor_node.attr('name');
                if (anchor_names.indexOf(anchor_name) == -1) {
                    let anchor_hash = '#' + anchor_name;
                    let link_selector = _this.options.selector.replace(/a\[name]/g, 'a[href="' + anchor_hash + '"]');
                    this.link_nodes = $(link_selector);
                    this.li_nodes = this.link_nodes.parents('li:first');
                    this.sticky_nodes = anchor_node.parent().prevAll('[data-sticky], .ghost');
                    if (anchor_hash == _this.hash) {
                        _this.scrollToAnchor(anchor_node);
                        _this.activateAnchor(anchor_node);
                    }
                    _this.anchor_nodes = _this.anchor_nodes.add(this);
                    _this.li_nodes = _this.li_nodes.add(this.li_nodes);
                    anchor_names.push(anchor_name);
                }
            }
        });
        this.anchor_nodes_reverse = $(this.anchor_nodes.get().reverse());
        this.setEvents();
    },
    setEvents: function() {
        let _this = this;
        this.anchor_nodes.each(function() {
            let anchor_node = $(this);
            this.link_nodes.on('click', function(event) {
                event.preventDefault();
                _this.scrollToAnchor(anchor_node, 500);
                _this.activateAnchor(anchor_node);
            });
        });
        if (this.li_nodes.length) {
            setTimeout(function() {
                _this.viewport_node.on('scroll', function() {
                    if (!_this.hold) {
                        let anchor_node = _this.getVisibleAnchor();
                        _this.activateAnchor(anchor_node);
                    }
                });
            }, 100);
        }
    },
    getVisibleAnchor: function() {
        let anchor_node = $();
        this.anchor_nodes_reverse.each(function() {
            let anchor_top = $(this).offset().top;
            this.sticky_nodes.each(function() {
                anchor_top -= $(this).outerHeight(true);
            });
            if (anchor_top <= 0) {
                anchor_node = $(this);
                return false;
            }
        });
        return anchor_node;
    },
    scrollToAnchor: function(anchor_node, duration) {
        let _this = this;
        let anchor_top = anchor_node.offset().top + this.viewport_node.scrollTop();
        anchor_node.prop('sticky_nodes').each(function() {
            anchor_top -= $(this).outerHeight(true);
        });
        anchor_top = Math.ceil(anchor_top);
        if (duration) {
            this.hold = true;
            this.viewport_node.animate({
                scrollTop: anchor_top
            }, duration, function() {
                _this.hold = false;
            });
        } else {
            this.viewport_node.scrollTop(anchor_top);
        }
    },
    activateAnchor: function(anchor_node) {
        let anchor_name = anchor_node.attr('name') || '';
        let hash = '#' + anchor_name;
        let li_nodes = anchor_node.prop('li_nodes');
        if (hash != this.hash) {
            this.hash = hash;
            history.replaceState(null, null, hash);
        }
        if (li_nodes && li_nodes.length) {
            li_nodes.siblings('.active').removeClass('active');
            li_nodes.addClass('active');
        } else {
            this.li_nodes.removeClass('active');
        }
    }
};
var Maps = function(options) {
    this.init(options);
};
Maps.prototype = {
    init: function(options) {
        this.options = {
            selector: '.map',
            lat: null,
            lng: null,
            address: 'Rotterdam',
            zoom: 11,
            zoom_control: false,
            map_type_control: false,
            street_view_control: false,
            rotate_control: false,
            scale_control: false,
            full_screen_control: false,
            scrollwheel: false,
            draggable: false,
            grayscale: false,
            icon: null,
            icon_position: [],
            info_window: null
        };
        $.extend(this.options, options);
        this.map_node = $();
        this.lat = 0;
        this.lng = 0;
        this.start();
    },
    start: function() {
        var _this = this;
        var selectors = this.options.selector.split(',');
        if (selectors.length > 1) {
            for (var x in selectors) {
                var options = $.extend({}, this.options);
                options.selector = $.trim(selectors[x]);
                new Maps(options);
            }
        } else {
            var map_nodes = $(this.options.selector);
            map_nodes.each(function(index) {
                if (index) {
                    var options = $.extend({}, _this.options);
                    options.selector += ':eq(' + index + ')';
                    new Maps(options);
                } else {
                    _this.map_node = $(this);
                    _this.setMap();
                }
            });
        }
    },
    setMap: function() {
        var _this = this;
        var lat = parseFloat(this.options.lat);
        var lng = parseFloat(this.options.lng);
        if (!isNaN(lat) && !isNaN(lng)) {
            var inner_node = $('<div>');
            var lat_lng = new google.maps.LatLng(lat,lng);
            var icon = null;
            var options = {
                zoom: this.options.zoom,
                center: lat_lng,
                zoomControl: this.options.zoom_control,
                mapTypeControl: this.options.map_type_control,
                streetViewControl: this.options.street_view_control,
                rotateControl: this.options.rotate_control,
                scaleControl: this.options.scale_control,
                fullscreenControl: this.options.full_screen_control,
                scrollwheel: this.options.scrollwheel,
                draggable: this.options.draggable
            };
            this.map_node.prepend(inner_node);
            if (this.options.icon) {
                icon = {
                    url: this.options.icon
                };
                if (this.options.icon_position.length == 2) {
                    var icon_left = this.options.icon_position[0];
                    var icon_top = this.options.icon_position[1];
                    icon.anchor = new google.maps.Point(icon_left,icon_top);
                }
            }
            var map = new google.maps.Map(inner_node.get(0),options);
            var marker = new google.maps.Marker({
                map: map,
                position: lat_lng,
                icon: icon
            });
            if (this.options.grayscale) {
                var style = [{
                    featureType: 'all',
                    elementType: 'all',
                    stylers: [{
                        saturation: -100
                    }]
                }];
                var map_type = new google.maps.StyledMapType(style);
                map.mapTypes.set('grayscale', map_type);
                map.setMapTypeId('grayscale');
            }
            if (this.options.info_window) {
                var info_window = new google.maps.InfoWindow({
                    content: this.options.info_window
                });
                google.maps.event.addListener(marker, 'click', function() {
                    info_window.open(map, marker);
                });
            }
        } else if (this.options.address) {
            var geo = new google.maps.Geocoder;
            geo.geocode({
                'address': this.options.address
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var location = results[0].geometry.location;
                    _this.options.lat = location.lat();
                    _this.options.lng = location.lng();
                    _this.setMap();
                }
            });
        } else {
            _this.options.lat = _this.lat;
            _this.options.lng = _this.lng;
            _this.setMap();
        }
    }
};

// When the user scrolls the page, execute myFunction
window.onscroll = function() {myFunction()};

// Get the header
var header = document.getElementById("menuBlock");

// Get the offset position of the navbar
var sticky = header.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
    if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}